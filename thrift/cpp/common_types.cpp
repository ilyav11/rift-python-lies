/**
 * Autogenerated by Thrift Compiler (1.0.0-dev)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "common_types.h"

#include <algorithm>
#include <ostream>

#include <thrift/TToString.h>



int _kLeafIndicationsValues[] = {
  LeafIndications::leaf_only,
  LeafIndications::leaf_only_and_leaf_2_leaf_procedures
};
const char* _kLeafIndicationsNames[] = {
  "leaf_only",
  "leaf_only_and_leaf_2_leaf_procedures"
};
const std::map<int, const char*> _LeafIndications_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(2, _kLeafIndicationsValues, _kLeafIndicationsNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

int _kTieDirectionTypeValues[] = {
  TieDirectionType::Illegal,
  TieDirectionType::South,
  TieDirectionType::North,
  TieDirectionType::DirectionMaxValue
};
const char* _kTieDirectionTypeNames[] = {
  "Illegal",
  "South",
  "North",
  "DirectionMaxValue"
};
const std::map<int, const char*> _TieDirectionType_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(4, _kTieDirectionTypeValues, _kTieDirectionTypeNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

int _kAddressFamilyTypeValues[] = {
  AddressFamilyType::Illegal,
  AddressFamilyType::AddressFamilyMinValue,
  AddressFamilyType::IPv4,
  AddressFamilyType::IPv6,
  AddressFamilyType::AddressFamilyMaxValue
};
const char* _kAddressFamilyTypeNames[] = {
  "Illegal",
  "AddressFamilyMinValue",
  "IPv4",
  "IPv6",
  "AddressFamilyMaxValue"
};
const std::map<int, const char*> _AddressFamilyType_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(5, _kAddressFamilyTypeValues, _kAddressFamilyTypeNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

int _kTIETypeTypeValues[] = {
  TIETypeType::Illegal,
  TIETypeType::TIETypeMinValue,
  TIETypeType::NodeTIEType,
  TIETypeType::PrefixTIEType,
  TIETypeType::TransitivePrefixTIEType,
  TIETypeType::PGPrefixTIEType,
  TIETypeType::KeyValueTIEType,
  TIETypeType::TIETypeMaxValue
};
const char* _kTIETypeTypeNames[] = {
  "Illegal",
  "TIETypeMinValue",
  "NodeTIEType",
  "PrefixTIEType",
  "TransitivePrefixTIEType",
  "PGPrefixTIEType",
  "KeyValueTIEType",
  "TIETypeMaxValue"
};
const std::map<int, const char*> _TIETypeType_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(8, _kTIETypeTypeValues, _kTIETypeTypeNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

int _kRouteTypeValues[] = {
  RouteType::Illegal,
  RouteType::RouteTypeMinValue,
  RouteType::Discard,
  RouteType::LocalPrefix,
  RouteType::SouthPGPPrefix,
  RouteType::NorthPGPPrefix,
  RouteType::NorthPrefix,
  RouteType::SouthPrefix,
  RouteType::TransitiveSouthPrefix,
  RouteType::RouteTypeMaxValue
};
const char* _kRouteTypeNames[] = {
  "Illegal",
  "RouteTypeMinValue",
  "Discard",
  "LocalPrefix",
  "SouthPGPPrefix",
  "NorthPGPPrefix",
  "NorthPrefix",
  "SouthPrefix",
  "TransitiveSouthPrefix",
  "RouteTypeMaxValue"
};
const std::map<int, const char*> _RouteType_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(10, _kRouteTypeValues, _kRouteTypeNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));


IEEE802_1ASTimeStampType::~IEEE802_1ASTimeStampType() throw() {
}


void IEEE802_1ASTimeStampType::__set_AS_sec(const int64_t val) {
  this->AS_sec = val;
}

void IEEE802_1ASTimeStampType::__set_AS_nsec(const int32_t val) {
  this->AS_nsec = val;
__isset.AS_nsec = true;
}

uint32_t IEEE802_1ASTimeStampType::read(::apache::thrift::protocol::TProtocol* iprot) {

  apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;

  bool isset_AS_sec = false;

  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I64) {
          xfer += iprot->readI64(this->AS_sec);
          isset_AS_sec = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->AS_nsec);
          this->__isset.AS_nsec = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  if (!isset_AS_sec)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  return xfer;
}

uint32_t IEEE802_1ASTimeStampType::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("IEEE802_1ASTimeStampType");

  xfer += oprot->writeFieldBegin("AS_sec", ::apache::thrift::protocol::T_I64, 1);
  xfer += oprot->writeI64(this->AS_sec);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.AS_nsec) {
    xfer += oprot->writeFieldBegin("AS_nsec", ::apache::thrift::protocol::T_I32, 2);
    xfer += oprot->writeI32(this->AS_nsec);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(IEEE802_1ASTimeStampType &a, IEEE802_1ASTimeStampType &b) {
  using ::std::swap;
  swap(a.AS_sec, b.AS_sec);
  swap(a.AS_nsec, b.AS_nsec);
  swap(a.__isset, b.__isset);
}

IEEE802_1ASTimeStampType::IEEE802_1ASTimeStampType(const IEEE802_1ASTimeStampType& other0) {
  AS_sec = other0.AS_sec;
  AS_nsec = other0.AS_nsec;
  __isset = other0.__isset;
}
IEEE802_1ASTimeStampType& IEEE802_1ASTimeStampType::operator=(const IEEE802_1ASTimeStampType& other1) {
  AS_sec = other1.AS_sec;
  AS_nsec = other1.AS_nsec;
  __isset = other1.__isset;
  return *this;
}
void IEEE802_1ASTimeStampType::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "IEEE802_1ASTimeStampType(";
  out << "AS_sec=" << to_string(AS_sec);
  out << ", " << "AS_nsec="; (__isset.AS_nsec ? (out << to_string(AS_nsec)) : (out << "<null>"));
  out << ")";
}


IPv4PrefixType::~IPv4PrefixType() throw() {
}


void IPv4PrefixType::__set_address(const IPv4Address val) {
  this->address = val;
}

void IPv4PrefixType::__set_prefixlen(const PrefixLenType val) {
  this->prefixlen = val;
}

uint32_t IPv4PrefixType::read(::apache::thrift::protocol::TProtocol* iprot) {

  apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;

  bool isset_address = false;
  bool isset_prefixlen = false;

  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->address);
          isset_address = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_BYTE) {
          xfer += iprot->readByte(this->prefixlen);
          isset_prefixlen = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  if (!isset_address)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  if (!isset_prefixlen)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  return xfer;
}

uint32_t IPv4PrefixType::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("IPv4PrefixType");

  xfer += oprot->writeFieldBegin("address", ::apache::thrift::protocol::T_I32, 1);
  xfer += oprot->writeI32(this->address);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("prefixlen", ::apache::thrift::protocol::T_BYTE, 2);
  xfer += oprot->writeByte(this->prefixlen);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(IPv4PrefixType &a, IPv4PrefixType &b) {
  using ::std::swap;
  swap(a.address, b.address);
  swap(a.prefixlen, b.prefixlen);
}

IPv4PrefixType::IPv4PrefixType(const IPv4PrefixType& other2) {
  address = other2.address;
  prefixlen = other2.prefixlen;
}
IPv4PrefixType& IPv4PrefixType::operator=(const IPv4PrefixType& other3) {
  address = other3.address;
  prefixlen = other3.prefixlen;
  return *this;
}
void IPv4PrefixType::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "IPv4PrefixType(";
  out << "address=" << to_string(address);
  out << ", " << "prefixlen=" << to_string(prefixlen);
  out << ")";
}


IPv6PrefixType::~IPv6PrefixType() throw() {
}


void IPv6PrefixType::__set_address(const IPv6Address& val) {
  this->address = val;
}

void IPv6PrefixType::__set_prefixlen(const PrefixLenType val) {
  this->prefixlen = val;
}

uint32_t IPv6PrefixType::read(::apache::thrift::protocol::TProtocol* iprot) {

  apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;

  bool isset_address = false;
  bool isset_prefixlen = false;

  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readBinary(this->address);
          isset_address = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_BYTE) {
          xfer += iprot->readByte(this->prefixlen);
          isset_prefixlen = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  if (!isset_address)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  if (!isset_prefixlen)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  return xfer;
}

uint32_t IPv6PrefixType::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("IPv6PrefixType");

  xfer += oprot->writeFieldBegin("address", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeBinary(this->address);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("prefixlen", ::apache::thrift::protocol::T_BYTE, 2);
  xfer += oprot->writeByte(this->prefixlen);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(IPv6PrefixType &a, IPv6PrefixType &b) {
  using ::std::swap;
  swap(a.address, b.address);
  swap(a.prefixlen, b.prefixlen);
}

IPv6PrefixType::IPv6PrefixType(const IPv6PrefixType& other4) {
  address = other4.address;
  prefixlen = other4.prefixlen;
}
IPv6PrefixType& IPv6PrefixType::operator=(const IPv6PrefixType& other5) {
  address = other5.address;
  prefixlen = other5.prefixlen;
  return *this;
}
void IPv6PrefixType::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "IPv6PrefixType(";
  out << "address=" << to_string(address);
  out << ", " << "prefixlen=" << to_string(prefixlen);
  out << ")";
}


IPAddressType::~IPAddressType() throw() {
}


void IPAddressType::__set_ipv4address(const IPv4Address val) {
  this->ipv4address = val;
__isset.ipv4address = true;
}

void IPAddressType::__set_ipv6address(const IPv6Address& val) {
  this->ipv6address = val;
__isset.ipv6address = true;
}

uint32_t IPAddressType::read(::apache::thrift::protocol::TProtocol* iprot) {

  apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->ipv4address);
          this->__isset.ipv4address = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readBinary(this->ipv6address);
          this->__isset.ipv6address = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t IPAddressType::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("IPAddressType");

  if (this->__isset.ipv4address) {
    xfer += oprot->writeFieldBegin("ipv4address", ::apache::thrift::protocol::T_I32, 1);
    xfer += oprot->writeI32(this->ipv4address);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.ipv6address) {
    xfer += oprot->writeFieldBegin("ipv6address", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeBinary(this->ipv6address);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(IPAddressType &a, IPAddressType &b) {
  using ::std::swap;
  swap(a.ipv4address, b.ipv4address);
  swap(a.ipv6address, b.ipv6address);
  swap(a.__isset, b.__isset);
}

IPAddressType::IPAddressType(const IPAddressType& other6) {
  ipv4address = other6.ipv4address;
  ipv6address = other6.ipv6address;
  __isset = other6.__isset;
}
IPAddressType& IPAddressType::operator=(const IPAddressType& other7) {
  ipv4address = other7.ipv4address;
  ipv6address = other7.ipv6address;
  __isset = other7.__isset;
  return *this;
}
void IPAddressType::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "IPAddressType(";
  out << "ipv4address="; (__isset.ipv4address ? (out << to_string(ipv4address)) : (out << "<null>"));
  out << ", " << "ipv6address="; (__isset.ipv6address ? (out << to_string(ipv6address)) : (out << "<null>"));
  out << ")";
}


IPPrefixType::~IPPrefixType() throw() {
}


void IPPrefixType::__set_ipv4prefix(const IPv4PrefixType& val) {
  this->ipv4prefix = val;
__isset.ipv4prefix = true;
}

void IPPrefixType::__set_ipv6prefix(const IPv6PrefixType& val) {
  this->ipv6prefix = val;
__isset.ipv6prefix = true;
}

uint32_t IPPrefixType::read(::apache::thrift::protocol::TProtocol* iprot) {

  apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->ipv4prefix.read(iprot);
          this->__isset.ipv4prefix = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->ipv6prefix.read(iprot);
          this->__isset.ipv6prefix = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t IPPrefixType::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("IPPrefixType");

  if (this->__isset.ipv4prefix) {
    xfer += oprot->writeFieldBegin("ipv4prefix", ::apache::thrift::protocol::T_STRUCT, 1);
    xfer += this->ipv4prefix.write(oprot);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.ipv6prefix) {
    xfer += oprot->writeFieldBegin("ipv6prefix", ::apache::thrift::protocol::T_STRUCT, 2);
    xfer += this->ipv6prefix.write(oprot);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(IPPrefixType &a, IPPrefixType &b) {
  using ::std::swap;
  swap(a.ipv4prefix, b.ipv4prefix);
  swap(a.ipv6prefix, b.ipv6prefix);
  swap(a.__isset, b.__isset);
}

IPPrefixType::IPPrefixType(const IPPrefixType& other8) {
  ipv4prefix = other8.ipv4prefix;
  ipv6prefix = other8.ipv6prefix;
  __isset = other8.__isset;
}
IPPrefixType& IPPrefixType::operator=(const IPPrefixType& other9) {
  ipv4prefix = other9.ipv4prefix;
  ipv6prefix = other9.ipv6prefix;
  __isset = other9.__isset;
  return *this;
}
void IPPrefixType::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "IPPrefixType(";
  out << "ipv4prefix="; (__isset.ipv4prefix ? (out << to_string(ipv4prefix)) : (out << "<null>"));
  out << ", " << "ipv6prefix="; (__isset.ipv6prefix ? (out << to_string(ipv6prefix)) : (out << "<null>"));
  out << ")";
}


PrefixSequenceType::~PrefixSequenceType() throw() {
}


void PrefixSequenceType::__set_timestamp(const IEEE802_1ASTimeStampType& val) {
  this->timestamp = val;
}

void PrefixSequenceType::__set_transactionid(const PrefixTransactionIDType val) {
  this->transactionid = val;
__isset.transactionid = true;
}

uint32_t PrefixSequenceType::read(::apache::thrift::protocol::TProtocol* iprot) {

  apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;

  bool isset_timestamp = false;

  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->timestamp.read(iprot);
          isset_timestamp = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_BYTE) {
          xfer += iprot->readByte(this->transactionid);
          this->__isset.transactionid = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  if (!isset_timestamp)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  return xfer;
}

uint32_t PrefixSequenceType::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("PrefixSequenceType");

  xfer += oprot->writeFieldBegin("timestamp", ::apache::thrift::protocol::T_STRUCT, 1);
  xfer += this->timestamp.write(oprot);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.transactionid) {
    xfer += oprot->writeFieldBegin("transactionid", ::apache::thrift::protocol::T_BYTE, 2);
    xfer += oprot->writeByte(this->transactionid);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(PrefixSequenceType &a, PrefixSequenceType &b) {
  using ::std::swap;
  swap(a.timestamp, b.timestamp);
  swap(a.transactionid, b.transactionid);
  swap(a.__isset, b.__isset);
}

PrefixSequenceType::PrefixSequenceType(const PrefixSequenceType& other10) {
  timestamp = other10.timestamp;
  transactionid = other10.transactionid;
  __isset = other10.__isset;
}
PrefixSequenceType& PrefixSequenceType::operator=(const PrefixSequenceType& other11) {
  timestamp = other11.timestamp;
  transactionid = other11.transactionid;
  __isset = other11.__isset;
  return *this;
}
void PrefixSequenceType::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "PrefixSequenceType(";
  out << "timestamp=" << to_string(timestamp);
  out << ", " << "transactionid="; (__isset.transactionid ? (out << to_string(transactionid)) : (out << "<null>"));
  out << ")";
}


